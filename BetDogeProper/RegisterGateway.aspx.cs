
using System;
using System.Web;
using System.Web.UI;

namespace BetDogeProper
{
	public partial class RegisterGateway : System.Web.UI.Page
	{
		public void Page_Load ()
		{
			HelperThings h = new HelperThings (Request, Response);
			if (Request.Form ["password"] != Request.Form ["prepeat"]) {
				h.Redirect ("/Register.aspx?error=" + HttpUtility.UrlEncode ("Your passwords don't match."));
				return;
			}
			string res = h.CreateUser (Request.Form ["username"], Request.Form ["password"]);
			if (res != "success") {
				h.Redirect ("/Register.aspx?error=" + HttpUtility.UrlEncode (res));
				return;
			}
			h.Redirect ("/Register.aspx?success=1");
		}
	}
}

