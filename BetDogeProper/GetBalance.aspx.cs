
using System;
using System.Web;
using System.Web.UI;

namespace BetDogeProper
{
	public partial class GetBalance : System.Web.UI.Page
	{
		public void Page_Load ()
		{
			HelperThings h = new HelperThings (Request, Response);
			if (!h.IsLoggedIn ()) {
				h.Redirect ("/Default.aspx");
				return;
			}
			Response.Write (h.GetUserBalance (h.GetUsername ()));
		}
	}
}

