
using System;
using System.Web;
using System.Web.UI;

namespace BetDogeProper
{
	public partial class Default : System.Web.UI.Page
	{
		public void Page_Load ()
		{
			HelperThings h = new HelperThings (Request, Response);
			if (h.IsLoggedIn ())
				h.Redirect ("/Bet.aspx");
			if (h.CookieExists ("noticeshown")) {
				notice.InnerHtml = "";
				return;
			}
			h.SetCookie ("noticeshown", "", 0xdead00);
		}
	}
}

