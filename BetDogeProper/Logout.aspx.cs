
using System;
using System.Web;
using System.Web.UI;

namespace BetDogeProper
{
	public partial class Logout : System.Web.UI.Page
	{
		public void Page_Load ()
		{
			HelperThings h = new HelperThings (Request, Response);
			if (!h.DestroySession ()) {
				Response.Write ("Logout operation failed. You will be redirected in 3 seconds...");
				Response.Write ("<META HTTP-EQUIV=\"REFRESH\" CONTENT=\"3;URL=/Default.aspx\">");
			} else {
				h.Redirect ("/Default.aspx");
			}
		}
	}
}

