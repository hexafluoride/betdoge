$('.slider').slider();
$('#sl1').slider().on('slide', updateText);
$('#sl1').slider('setValue', 50);
updateText_2('50');
function updateText(ev) {
	updateText_2(ev.value);
}
function updateText_2(e) {
	amount = parseFloat($('#amount').val());
	val = parseInt(e);
	multiplier = (100 / (val + 2));
	$('#ruw').text(e);
	$('#multiplier').attr('value', multiplier.toPrecision(4));
	$('#odds2').attr('value', e + '%');
	$('#profit').attr('value', ((amount * multiplier) - amount).toPrecision(7));
}
function updateProfit() {
	amount = parseFloat($('#amount').val());
	$('#profit').attr('value', (((100 / (parseInt($('#ruw').text()) + 2)) * amount) - amount).toPrecision(7));
}