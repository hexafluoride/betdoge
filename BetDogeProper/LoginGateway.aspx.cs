
using System;
using System.Web;
using System.Web.UI;

namespace BetDogeProper
{
	public partial class LoginGateway : System.Web.UI.Page
	{
		public void Page_Load()
		{
			HelperThings help = new HelperThings(Request, Response);
			string username = Request.Form["username"];
			string password = Request.Form["password"];
			string s = help.GenerateSession (username, password);
			if (s == "-") {
				Response.Redirect ("/Login.aspx?error=1");
			} else {
				help.SetCookie ("username", username);
				help.SetCookie ("sessionid", s);
				help.Redirect ("/Default.aspx");
			}
		}
	}
}

