<%@ Page Language="C#" Inherits="BetDogeProper.Default" MasterPageFile="~/MasterPage.master" %>
<asp:Content runat="server" id="cont" ContentPlaceHolderID="contentPlaceHolder">
<div id="notlog" runat="server">
      <div class="hero-unit">
        <h1>BetDoge</h1>
        <p>Welcome to the new way to roll.</p>
        <p><a href="#" class="btn btn-primary btn-large">Sign up now</a></p>
      </div>

      <!-- Example row of columns -->
      <div class="row">
        <div class="span4">
          <h2>Probably fair</h2>
          <p>BetDoge is statistically fair with a house edge of 2%.</p>
          <p><a class="btn" disabled href="/Stats.aspx">View statistics &raquo;</a></p>
        </div>
        <div class="span4">
          <h2>Start now</h2>
          <p>We accept deposits instantly, requiring no confirmations.</p>
          <p><a class="btn" href="/Register.aspx">Register now for free &raquo;</a></p>
       </div>
        <div class="span4">
          <h2>Register in 5 seconds</h2>
          <p>We don't want your e-mail. We don't want you to enter squiggly words. We just want you.</p>
          <p><a class="btn" href="/Register.aspx">Register now for free &raquo;</a></p>
        </div>
      </div>
</div>
<div id="notice" runat="server">
	<div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	  <div class="modal-header">
	    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
	    <h3 id="myModalLabel">Notice</h3>
	  </div>
	  <div class="modal-body">
	    <p><b>By clicking the button labeled "I accept", dismissing this notice or using this service, you accept the following:</b><br />
	    <br />1. I am at least 18 years of age.
	    <br />2. I am accessing this service from jurisdictions which this service is legal.
	    <br />3. I understand the risks of gambling.
	    <br /><br />If you do not meet the conditions, please stop using this service immediately.</p>
	  </div>
	  <div class="modal-footer">
	    <button class="btn" data-dismiss="modal" aria-hidden="true">I accept</button>
	  </div>
	</div>
	<script lang="javascript">
	function showNotice()
	{
		$('#myModal').modal();
	}
	</script>
</div>
<!--
<div id="log" runat="server">
<table class="table table-striped">
        <tbody id="table" runat="server"><tr>
            <th>Username</th>
            <th>ID</th>
            <th>Amount</th>
            <th>Time</th>
        </tr>
        
    </tbody></table>
</div>-->
</asp:Content>
