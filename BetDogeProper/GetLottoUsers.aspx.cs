using System.Linq;
using System;
using System.Web;
using System.Web.UI;

namespace BetDogeProper
{
	public partial class GetLottoUsers : System.Web.UI.Page
	{
		public void Page_Load ()
		{
			HelperThings h = new HelperThings (Request, Response);
			if (!Request.Params.AllKeys.Contains ("type"))
				return;
			if (Request.Params ["type"] == "users") {
				Response.Write (h.GetLottoUsers ());
			} else if (Request.Params ["type"] == "total") {
				Response.Write (h.GetLottoTotal ().ToString ("0.00"));
			} else if (Request.Params ["type"] == "timeleft") {
				Response.Write (h.GetTimeLeft ());
			} else if (Request.Params ["type"] == "lastwinner") {
				Response.Write (h.GetLastWinner ());
			}
		}
	}
}

