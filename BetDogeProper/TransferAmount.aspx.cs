using MySql.Data.MySqlClient;
using System;
using System.Web;
using System.Web.UI;
using System.Linq;

namespace BetDogeProper
{
	public partial class TransferAmount : System.Web.UI.Page
	{
		public void Page_Load ()
		{
			HelperThings h = new HelperThings (Request, Response);
			if (!h.IsLoggedIn ()) {
				h.Redirect ("/Default.aspx");
				return;
			}
			if (!Request.Form.AllKeys.Contains ("amount")) {
				h.Redirect ("/Default.aspx");
				return;
			}
			float amount = 0;
			if (!float.TryParse (Request.Form ["amount"], out amount)) {
				Response.Write ("{\"result\":\"Invalid amount.\"}");
				return;
			}
			if (amount > h.GetUserBalance (h.GetUsername ())) {
				Response.Write ("{\"result\":\"Your balance is too low!\"}");
				return;
			}
			if (amount < 0) {
				Response.Write ("{\"result\":\"Invalid amount.\"}");
				return;
			}
			string uname = h.GetUsername ();
			MySqlCommand cl = new MySqlCommand("SELECT * FROM lottery_users WHERE username=?username", h.conn);
			cl.Parameters.Add (new MySqlParameter("?username", uname));
			MySqlDataReader read = cl.ExecuteReader ();
			bool r = read.HasRows;
			read.Close ();
			if(r)
			{
				MySqlCommand updl = new MySqlCommand ("UPDATE lottery_users SET balance=balance+?amount WHERE username=?username", h.conn);
				updl.Parameters.Add (new MySqlParameter ("?username", uname));
				updl.Parameters.Add (new MySqlParameter ("?amount", amount));
				updl.ExecuteNonQuery ();
			}
			else
			{
				MySqlCommand updl = new MySqlCommand ("INSERT INTO lottery_users VALUES(?username, ?balance)", h.conn);
				updl.Parameters.Add (new MySqlParameter ("?username", uname));
				updl.Parameters.Add (new MySqlParameter ("?balance", amount));
				updl.ExecuteNonQuery ();
			}
			MySqlCommand final = new MySqlCommand("UPDATE users SET balance=balance-?amount WHERE username=?username", h.conn);
			final.Parameters.Add (new MySqlParameter("?amount", amount));
			final.Parameters.Add (new MySqlParameter("?username", uname));
			final.ExecuteNonQuery ();
			Response.Write ("{\"result\":\"success\", \"balance\":\"" + h.GetUserBalance (uname) + "\"}");
		}
	}
}

