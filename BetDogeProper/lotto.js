function updateTimeLeft() {
	$.get("/LottoInfo.aspx?type=timeleft", function(data) {
		$("#timeleft").text(data + " seconds");
	});
}
function updateList() {
	$.get("/LottoInfo.aspx?type=users", function(data) {
		$("#list").html("<tr><th>Username</th><th>Number of tickets</th><th>Chance to win</th><th>Wins if roll is</th></tr>" + data);
	});
}
function updateLastWinner() {
	$.get("/LottoInfo.aspx?type=lastwinner", function(data) {
		$("#lastwinner").html(data);
	});
}
function updateTotal() {
	$.get("/LottoInfo.aspx?type=total", function(data) {
		$("#total").text(data + " DOGE");
	});
}
function updateFast() {
	updateTimeLeft();
	updateList();
	updateTotal();
}
function updateSlow() {
	updateLastWinner();
}
function updateAll() {
	updateTimeLeft();
	updateList();
	updateLastWinner();
	updateTotal();
}
function transferAmount(){
  if(isNaN($('#amount').val()) || parseInt($('#amount').val()) < 0 || $('#amount').val() == '') {
  	$('#ac').attr("class", "control-group error");
  	$('#amount').trigger('focus');
  	return;
  }
  $('#ac').attr("class", "control-group");
  $.post("/TransferAmount.aspx",
  {
    amount:$("#amount").val()
  },
  function(data,status){
  	var json = JSON.parse(data);
  	if(json.result != "success")
  	{
  		alert(json.result);
  		return;
  	}
  	$("b[name='balance']").text(json.balance);
  	updateFast();
  });
}
function updateBalance(){
	$.get("/GetBalance.aspx", function(data) {
		$('#balance').text(data);
	});
}

setInterval(updateFast, 2500);
setInterval(updateSlow, 7500);
updateAll();
updateBalance();