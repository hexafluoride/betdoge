<%@ Page Language="C#" Inherits="BetDogeProper.Lottery" MasterPageFile="~/MasterPage.master" %>
<asp:Content runat="server" id="cont" ContentPlaceHolderID="contentPlaceHolder">
	<div class="container well well-small span12" style="margin-left:0px;">
	<p>
		<h1>Dogecoin Lottery</h1><br />
		<b>How does it work?</b><br />
		Every DOGE counts as one ticket, one ticket = one chance to win.<br />
		At the end of this round the system will generate a random number between 0 and the total amount of tickets.<br />
		If you win you will get 95% of the DOGE collected.<br />
		The lottery occurs every day(24 hours or 86400 seconds).<br />
		<br />
		<b>How do I deposit DOGE?</b><br />
		Send your DOGE to the address <b id="addr" runat="server"></b> or click <a href="#myModal" role="button" data-toggle="modal">here</a> to transfer DOGE from your balance.<br />
		<br />
		<b>When does this round end?</b><br />
		This round ends in <b id="timeleft">30 minutes</b>.<br />
		<br />
		<h2>List of players</h2>
		<table class="table table-striped table-condensed" id="list">
			<tr>
				<th>Username</th>
				<th>Number of tickets</th>
				<th>Chance to win</th>
				<th>Wins if roll is</th>
			</tr>
		</table>
		The winner of this round will get <b id="total">95% of the total amount</b>.<br />
		<br />
		<small id="lastwinner"></small>
	</p>
	</div>

	<div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	    <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
	        <h3 id="myModalLabel">Transfer DOGE</h3>
	    </div>
	    <div class="modal-body">
	        <p>Your current balance is <b id="balance" name="balance"></b> DOGE.<br />
	        Transfer <span id="ac" class="control-group"><input type="text" id="amount" style="margin-top:6px;" placeholder="amount" /></span> DOGE from my account to the lottery.
	        </p>
	    </div>
	    <div class="modal-footer">
	        <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
	        <button class="btn btn-success" onclick="transferAmount()">Confirm</button>
	    </div>
	</div>
	<script src="lotto.js"></script>
</asp:Content>
