
using System;
using System.Linq;
using System.Web;
using System.Web.UI;

namespace BetDogeProper
{
	public partial class Withdraw : System.Web.UI.Page
	{
		public void Page_Load ()
		{
			HelperThings h = new HelperThings (Request, Response);
			if (!h.IsLoggedIn ()) {
				h.Redirect ("/Default.aspx");
				return;
			}
			balance.InnerText = h.GetUserBalance (h.GetCookie ("username")).ToString ();
			if (Request.Params.AllKeys.Contains ("error")) {
				switch (Request.Params ["error"]) {
				case "1":
					pc.Attributes ["class"] += " error";
					pcs.InnerHtml += "<span class\"help-inline\">Your password isn't correct.</span>";
					break;
				case "2":
					ac.Attributes ["class"] += " error";
					acs.InnerHtml += "<span class\"help-inline\">Invalid amount.</span>";
					break;
				case "3":
					ac.Attributes ["class"] += " error";
					acs.InnerHtml += "<span class\"help-inline\">Insufficient funds.</span>";
					break;
				case "4":
					bc.Attributes ["class"] += " error";
					bcs.InnerHtml += "<span class\"help-inline\">We are sorry, but we can't process this request right now.</span>";
					break;
				case "5":
					adc.Attributes ["class"] += " error";
					adcs.InnerHtml += "<span class\"help-inline\">Invalid address.</span>";
					break;
				default:
					break;
				}
			} else if (Request.Params.AllKeys.Contains ("txid")) {
				bc.Attributes ["class"] += " success";
				bcs.InnerHtml += "<span class\"help-inline\">Success! You can track your transaction <a href=\"http://dogechain.info/tx/" + HttpUtility.HtmlEncode (Request.Params["txid"]) + "\">here</a>.</span>";
			}
		}
	}
}

