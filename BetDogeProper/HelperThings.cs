using System;
using MySql.Data.MySqlClient;
using Bitnet.Client;
using System.Net;
using System.IO;
using System.Linq;
using System.Web;
using System.Collections.Generic;
using System.Security.Cryptography;

namespace BetDogeProper
{
	public class HelperThings
	{
		public MySqlConnection conn = new MySqlConnection("Server=localhost;Port=3306;Database=betdoge;Uid=root;Pwd=lala;Pooling=false;");
		public BitnetClient bc = new BitnetClient("http://localhost:22555/");
		HttpRequest req;
		Random rnd = new Random();
		HttpResponse resp;
		const string SITEMAP_TEMP = "<li><a href=\"/{1}\">{0}</a></li>";
		const string SITEMAP_ACTIVE_TEMP = "<li class=\"active\"><a href=\"/{1}\">{0}</a></li>";
		Dictionary<string, string> userpages = new Dictionary<string, string>(){
			{"Bet", "Bet.aspx"},
			{"Lottery", "Lottery.aspx"},
			{"Withdraw", "Withdraw.aspx"},
			{"Deposit", "Deposit.aspx"}};
		Dictionary<string, string> guestpages = new Dictionary<string, string>(){
			{"Home", "Default.aspx"}};
		public void SetCookie (string name, string val, int emin = -1)
		{
			resp.SetCookie (new HttpCookie(name, val){
				Expires = DateTime.Now.Add (new TimeSpan(0, 
				                                         emin == -1 ? 12345678 : emin, 
				                                         0))
			});
		}
		public bool UserExists (string username)
		{
			MySqlCommand comm = new MySqlCommand("SELECT * FROM users WHERE username=?username", conn);
			comm.Parameters.Add (new MySqlParameter("?username", username));
			MySqlDataReader reader = comm.ExecuteReader ();
			bool ret = reader.HasRows;
			reader.Close ();
			return ret;
		}
		public string GetUsername()
		{
			return this.GetCookie ("username");
		}
		public string CreateUser (string username, string password)
		{
			if(password.Length < 6)
				return "Password too weak.";
			username = HttpUtility.HtmlEncode (username);
			if(UserExists (username))
				return "Username not available.";
			if(username.Length == 0 || username.Length > 30)
				return "Invalid username.";
			if(username.EndsWith ("_lotto"))
				return "Username is reserved.";
			password = Hash (password);
			MySqlCommand insert = new MySqlCommand("INSERT INTO users VALUES(NULL, ?username, ?password, 0)", conn);
			insert.Parameters.Add (new MySqlParameter("?username", username));
			insert.Parameters.Add (new MySqlParameter("?password", password));
			insert.ExecuteNonQuery ();
			return "success";
		}
		public void DestroyCookie (string name)
		{
			if(!CookieExists (name))
				return;
			resp.Cookies[name].Expires = DateTime.Now.AddDays (-7);
		}
		public HelperThings (HttpRequest request, HttpResponse response)
		{
			req = request;
			resp = response;
			conn.Open ();
			bc.Credentials = new NetworkCredential ("dogecoinrpc", "2y2y4TYBACPSVFFhy1BSn5VgnzVixYHqkz1Rxduhvqae");
		}
		public string GetDepositAddress ()
		{
			string username = GetCookie ("username");
			if(bc.GetAddressesByAccount (username).Count() == 0)
				bc.GetNewAddress (username);
			return bc.GetAddressesByAccount (username).ToList ()[0];
		}
		public string GetNavbarForm (bool loggedin)
		{
			if (loggedin) {
				return "<a href=\"/Logout.aspx\" class=\"btn btn-primary\" style=\"margin-top:10px;\">Log out</a>";
			} else {
				return "<a href=\"/Login.aspx\" class=\"btn btn-success\" style=\"margin-top:10px; margin-right:5px;\">Sign in</a>" +
					"<a href=\"/Register.aspx\" class=\"btn btn-success\" style=\"margin-top:10px;\">Sign up</a>";
			}
		}
		public bool CookieExists (string cookie)
		{
			return req.Cookies[cookie] != null;
		}
		public string GetCookie (string cookie)
		{
			return req.Cookies[cookie].Value;
		}
		public string GetSiteMapString (string objname, string path)
		{
			if (req.Path.Contains (objname)) {
				return string.Format (SITEMAP_ACTIVE_TEMP, objname, path);
			} else {
				return string.Format (SITEMAP_TEMP, objname, path);
			}
		}
		public static DateTime GetDateTime( double unixTimeStamp )
		{
		    // Unix timestamp is seconds past epoch
		    System.DateTime dtDateTime = new DateTime(1970,1,1,0,0,0,0);
		    dtDateTime = dtDateTime.AddSeconds( unixTimeStamp ).ToLocalTime();
		    return dtDateTime;
		}
		public string GetBets (int count, bool hidden = true)
		{
			string ret = "";
			MySqlCommand comm = new MySqlCommand ("SELECT * FROM bets ORDER BY time DESC", conn);
			MySqlDataReader reader = comm.ExecuteReader ();
			for (int i = 0; i < count; i++) {
				reader.Read ();
				double amount = reader.GetDouble (2);
				float chance = reader.GetFloat (5);
				float multiplier = (100 / (chance + 2));
				bool win = reader.GetBoolean(3);
				string res = "";
				if(win)
					res = "+" + ((amount * multiplier) - amount).ToString ("0.00");
				else
					res = "-" + amount.ToString ("0.00");
				ret += "<tr class=\"" + (win ? "success" : "error") + "\"" + (hidden ? " style=\"height:0px;\" " : "") + ">" +
					"<td>" + reader.GetInt32 (0) +
					"</td><td>" + reader.GetString (1) +
					"</td><td>" + amount +
					"</td><td>" + chance +
					"</td><td>" + GetDateTime (reader.GetInt32(4)).ToString ("dd MMM yyyy HH:mm:ss") +
					"</td><td>" + res +  "</tr>\n";
			}
			return ret;
		}
		public string Hash(string str)
		{
			SHA512Managed man = new SHA512Managed();
			return BitConverter.ToString (man.ComputeHash (System.Text.Encoding.ASCII.GetBytes (str))).Replace ("-", "").ToUpper ();
		}
		public bool DestroySession ()
		{
			if(!IsLoggedIn ())
				return false;
			string username = GetCookie ("username");
			string sessionid = GetCookie ("sessionid");
			DestroyCookie ("username");
			DestroyCookie ("sessionid");
			MySqlCommand comm = new MySqlCommand("DELETE FROM sessions WHERE username=?username AND sessionid=?sessionid", conn);
			comm.Parameters.Add (new MySqlParameter("?username", username));
			comm.Parameters.Add (new MySqlParameter("?sessionid", sessionid));
			comm.ExecuteNonQuery ();
			return true;
		}
		public bool CheckUserPass (string username, string password)
		{
			MySqlCommand comm = new MySqlCommand("SELECT * FROM users WHERE username=?username AND password=?password", conn);
			comm.Parameters.Add (new MySqlParameter("?username", username));
			comm.Parameters.Add (new MySqlParameter("?password", Hash (password)));
			MySqlDataReader reader = comm.ExecuteReader ();
			bool ret = reader.HasRows;
			reader.Close ();
			return ret;
		}
		public float GetUserBalance (string username)
		{
			MySqlCommand comm = new MySqlCommand("SELECT balance FROM users WHERE username=?username", conn);
			comm.Parameters.Add (new MySqlParameter("?username", username));
			MySqlDataReader reader = comm.ExecuteReader ();
			if(!reader.HasRows)
				return -1;
			reader.Read ();
			float ret = (float)reader.GetDouble (0);
			reader.Close ();
			return ret;
		}
		public int GetTimeLeft ()
		{
			int start = int.Parse (File.ReadAllText ("/root/lotto_start"));
			int len = int.Parse (File.ReadAllText ("/root/lotto_length"));
			return (start + len) - GetTimeStamp ();
		}
		public string GetLastWinner ()
		{
			MySqlCommand comm = new MySqlCommand("SELECT * FROM lottery_winners ORDER BY ptime DESC", conn);
			MySqlDataReader reader = comm.ExecuteReader ();
			if(!reader.HasRows)
				return "";
			reader.Read ();
			return "<b>" + reader.GetString (0) + "</b> won the last lotto, winning <b>"  + reader.GetFloat (1) + "</b> DOGE by rolling <b>" + reader.GetFloat (3) + "</b>.";
		}
		public double GetLottoTotal(bool win = true)
		{
			MySqlCommand gt = new MySqlCommand("SELECT SUM(balance) FROM lottery_users", conn);
			var o = gt.ExecuteScalar();
			if(o == DBNull.Value)
				return 0;
			return (double)o * (win ? 0.95 : 1);
		}
		public string GetLottoUsers ()
		{
			string ret = "";
			double total = GetLottoTotal (false);
			if(total == 0)
				return "";
			MySqlCommand comm = new MySqlCommand("SELECT * FROM lottery_users", conn);
			MySqlDataReader reader = comm.ExecuteReader ();
			string line = "</td><td>";
			float t = 0;
			while(reader.Read ())
			{
				string username = reader.GetString (0);
				float balance = reader.GetFloat (1);
				string chance = ((balance / total) * 100).ToString ("0.00") + "%";
				string roll = ">" + t.ToString ("0.00") + " and <" + (t + balance).ToString("0.00");
				ret += "<tr><td>" + username + line +
						balance + line +
						chance + line +
						roll + "</td></tr>";
				t += balance;
			}
			return ret;
		}
		public string Bet (string username, float odds, float amount)
		{
			if(GetUserBalance (username) < amount)
				return "{\"result\":\"Your balance is too low!\"}";
			if(odds > 90)
				return "{\"result\":\"Odds must be lower than 90%.\"}";
			if(odds < 3)
				return "{\"result\":\"Odds must be higher than 3%.\"}";
			if(amount < 10)
				return "{\"result\":\"Minimum betting amount is 10 DOGE.\"}";
			bool win = rnd.Next (100) < odds - 2;
			float wamount = amount * 100 / (odds + 2);
			RegisterBet (username, win, amount, odds);
			if(win)
				UpdateBalance (username, wamount - amount);
			else
				UpdateBalance (username, -amount);
			return "{\"result\":\"success\", \"win\":\"" + (win ? "true" : "false") + "\", \"balance\":" + GetUserBalance (username) + "}";
		}
		public void RegisterBet (string username, bool win, float amount, float odds)
		{
			MySqlCommand comm = new MySqlCommand ("INSERT INTO bets VALUES(NULL, ?username, ?amount, ?win, ?time, ?chance)", conn);
			comm.Parameters.Add (new MySqlParameter ("?username", username));
			comm.Parameters.Add (new MySqlParameter ("?amount", amount));
			comm.Parameters.Add (new MySqlParameter ("?win", win));
			comm.Parameters.Add (new MySqlParameter ("?time", GetTimeStamp ()));
			comm.Parameters.Add (new MySqlParameter ("?chance", odds));
			comm.ExecuteNonQuery ();
		}
		public void UpdateBalance (string username, float amount)
		{
			MySqlCommand comm = new MySqlCommand ("UPDATE users SET balance=balance+?amount WHERE username=?username", conn);
			comm.Parameters.Add (new MySqlParameter("?amount", amount));
			comm.Parameters.Add (new MySqlParameter("?username", username));
			comm.ExecuteNonQuery ();
		}
		public int GetTimeStamp ()
		{
			return DateTimeToUnixTimestamp (DateTime.Now);
		}
		public int DateTimeToUnixTimestamp(DateTime dateTime)
		{
			return (int)(dateTime - new DateTime(1970, 1, 1).ToLocalTime()).TotalSeconds;
		}
		public void Redirect (string loc)
		{
			resp.Redirect (loc);
		}
		public string RandomString ()
		{
			string str = "";
			for (int i = 0; i < 16; i++) {
				str += (char)rnd.Next (65, 65 + 26);
			}
			return str;
		}
		public string GenerateSession (string username, string password)
		{
			if (!CheckUserPass (username, password)) {
				return "-";
			}
			MySqlCommand comm = new MySqlCommand("INSERT INTO sessions VALUES(?sid, ?username)", conn); 
			string sessionid = Hash (RandomString());
			comm.Parameters.Add (new MySqlParameter("?sid", sessionid));
			comm.Parameters.Add (new MySqlParameter("?username", username));
			comm.ExecuteNonQuery ();
			return sessionid;
		}
		public string GetSiteMap(bool loggedin = false)
		{
			string ret = "";
			if(loggedin)
				userpages.ToList ().ForEach (t => ret += GetSiteMapString (t.Key, t.Value));
			else
				guestpages.ToList ().ForEach (t => ret += GetSiteMapString (t.Key, t.Value));
			return ret;
		}
		public bool IsLoggedIn()
		{
			if(!CookieExists ("username") || 
			   !CookieExists ("sessionid"))
				return false;
			MySqlCommand cmd = new MySqlCommand("SELECT * FROM sessions WHERE sessionid=?sid AND username=?uname", conn);
			cmd.Parameters.Add ("?sid", GetCookie("sessionid"));
			cmd.Parameters.Add ("?uname", GetCookie("username"));
			MySqlDataReader reader = cmd.ExecuteReader ();
			bool ret = reader.HasRows;
			reader.Close ();
			return ret;
		}
	}
}

