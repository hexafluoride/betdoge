using System.Linq;
using System;
using System.Web;
using System.Web.UI;

namespace BetDogeProper
{
	public partial class Register : System.Web.UI.Page
	{
		public void Page_Load ()
		{
			if (Request.Params.AllKeys.Contains ("error")) {
				alert.Attributes ["class"] = "alert alert-error";
				alert.InnerHtml = "<strong>Error!</strong> " + HttpUtility.HtmlEncode (Request.Params["error"]);
			} else if (Request.Params.AllKeys.Contains ("success")) {
				alert.Attributes ["class"] = "alert alert-success";
				alert.InnerHtml = "<strong>Success!</strong> You can log in to your account <a href=\"/Login.aspx\">here</a>.";
			}
		}
	}
}

