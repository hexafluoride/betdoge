
using System;
using System.Web;
using System.Web.UI;

namespace BetDogeProper
{
	public partial class Deposit : System.Web.UI.Page
	{
		public void Page_Load()
		{
			HelperThings h = new HelperThings(Request, Response);
			balance.InnerText = h.GetUserBalance (h.GetCookie ("username")).ToString ();
			addr.InnerText = h.GetDepositAddress ();
		}
	}
}

