using Newtonsoft.Json;
using System;
using System.Web;
using System.Web.UI;

namespace BetDogeProper
{
	public partial class WithdrawGateway : System.Web.UI.Page
	{
		public void Page_Load ()
		{
			HelperThings h = new HelperThings (Request, Response);
			if (!h.IsLoggedIn ()) {
				h.Redirect ("/Default.aspx");
				return;
			}
			
			if (!h.CheckUserPass (h.GetCookie ("username"), Request.Form ["password"])) {
				h.Redirect ("/Withdraw.aspx?error=1");
				return;
			}
			
			float amount = 0;
			if (!float.TryParse (Request.Form ["amount"], out amount) || amount < 0) {
				h.Redirect ("/Withdraw.aspx?error=2");
				return;
			}
			
			if (amount > h.GetUserBalance (h.GetCookie ("username"))) {
				h.Redirect ("/Withdraw.aspx?error=3");
				return;
			}
			
			if (Request.Form ["address"].Length < 10) {
				h.Redirect ("/Withdraw.aspx?error=5");
				return;
			}
			
			h.UpdateBalance (h.GetCookie ("username"), -amount);
			string txid = h.bc.SendToAddress (Request.Form["address"], amount * 0.98f, "", "");
			h.Redirect ("/Withdraw.aspx?txid=" + txid);
			
		}
	}
}

