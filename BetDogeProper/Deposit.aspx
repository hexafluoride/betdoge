<%@ Page Language="C#" Inherits="BetDogeProper.Deposit" MasterPageFile="~/MasterPage.master" %>
<asp:Content runat="server" id="cont" ContentPlaceHolderID="contentPlaceHolder">
Deposit your DOGE to the address <b id="addr" runat="server"></b>. Your DOGE will be added to your account instantly, requiring no confirmations.<br />
You can use this address for later deposits, too.

	<div class="well pull-right span2">
		<b>Your balance is <h1 id="balance" runat="server"></h1> DOGE.</b>
	</div>
</asp:Content>