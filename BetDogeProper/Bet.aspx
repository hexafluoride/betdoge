<%@ Page Language="C#" Inherits="BetDogeProper.Bet" MasterPageFile="~/MasterPage.master" %>
<asp:Content runat="server" id="cont" ContentPlaceHolderID="contentPlaceHolder">
	<div class="container well well-small span9">
		<div class="row">
			<div class="span2">
				<b class="text-center">
					ROLL UNDER <span style="color:#4c0;" id="ruw">50</span> TO WIN<br />
					<input type="text" id="sl1" data-slider-min="3" data-slider-max="90" value="50" />
				</b>
			</div>
			<div class="span2 offset1">
				<b class="text-center">
					MULTIPLIER<br />
					<input type="text" readonly id="multiplier" value="" />
				</b>
			</div>
			<div class="span2 offset1">
				<b class="text-center">
					CHANCE TO WIN<br />
					<input type="text" readonly id="odds2" value="" />
				</b>
			</div>
		</div>
		<div class="row">
			<div class="span2 control-group" style="margin-bottom: 0px;" id="ac">
				<b class="text-center">
					BET AMOUNT<br />
					<input type="text" value="10" placeholder="Your bet amount" onkeyup="updateProfit()" id="amount" />
				</b>
			</div>
			<div class="span2 offset1">
				<b class="text-center">
					PROFIT<br />
					<input type="text" readonly id="profit" value="" />
				</b>
			</div>
			<div class="span2 offset1">
				<button onclick="bet()" class="btn btn-success btn-large btn-block">ROLL</button>
			</div>
		</div>

	</div>

	<div class="well pull-right span2">
		<b>Your balance is <h1 id="balance" runat="server"></h1> DOGE.</b>
	</div>
	<div class="tabbable"> <!-- Only required for left/right tabs -->
	  <ul class="nav nav-tabs">
	    <li class="active"><a href="#tab1" data-toggle="tab">Bets</a></li>
	    <li><a href="#tab2" data-toggle="tab">Chat</a></li>
	  </ul>
	  <div class="tab-content">
	    <div class="tab-pane active" id="tab1" style="overflow:hidden;">
	      <table class="table table-striped span12" id="bets" style="margin-left:15px; margin-right:15px;"></table>
	    </div>
	    <div class="tab-pane" id="tab2">
	    <b class="text-center">ANYONE CAN LOG IN TO CHAT AS ANYONE, DO NOT TRUST ANYONE</b>
	      <iframe src="http://webchat.freenode.net?nick=replace&channels=%23dogecoin%2C%23betdoge&uio=MTE9MjM20f" runat="server" id="chat" class="span12" style="margin-left:0;" height="500" border="0"></iframe>
	    </div>
	  </div>
	</div>
	
	<script src="/sh.js"></script>
	
</asp:Content>