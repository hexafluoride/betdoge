
using System;
using System.Web;
using System.Web.UI;

namespace BetDogeProper
{
	public partial class MasterPage : System.Web.UI.MasterPage
	{

		public void Page_Load()
		{
			HelperThings h = new HelperThings(Request, Response);
			bool log = h.IsLoggedIn ();
			sitemap.InnerHtml = h.GetSiteMap (log);
			logpart.InnerHtml = h.GetNavbarForm (log);
		}

	}
}

