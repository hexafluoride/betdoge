updateTable();
setInterval(updateTable, 5000);

function bet(){
  if(isNaN($('#amount').val()) || parseInt($('amount').val()) < 0 || $('#amount').val() == '') {
  	$('#ac').attr("class", "span2 control-group error");
  	$('#amount').trigger('focus');
  	return;
  }
  $('#ac').attr("class", "span2 control-group");
  $.post("/BetGateway.aspx",
  {
    amount:$("#amount").val(),
    odds:$('#ruw').text()
  },
  function(data,status){
  	var json = JSON.parse(data);
  	if(json.result != "success")
  	{
  		alert(json.result);
  		return;
  	}
  	if(json.win == "true") {
  		$("h1").text(json.balance);
  		$("h1").attr("style", "color:#0c0");
  		$("h1").animate({color:'#000000'});
  	} else {
  		$("h1").text(json.balance);
  		$("h1").attr("style", "color:#c00");
  		$("h1").animate({color:'#000000'});
  	}
  });
  updateTable();
}

function updateTable() {
	$.get("/GetBets.aspx?count=7", function(data) {
		$('#bets').html('<tr><th>Bet ID</th><th>Username</th><th>Amount</th><th>Chance</th><th>When</th><th>Result</th></tr>' + data);
	});
}