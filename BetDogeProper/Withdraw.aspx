<%@ Page Language="C#" Inherits="BetDogeProper.Withdraw" MasterPageFile="~/MasterPage.master" %>
<asp:Content runat="server" id="cont" ContentPlaceHolderID="contentPlaceHolder">
<div class="row">
	<form action="/WithdrawGateway.aspx" method="POST" class="form-horizontal span4">
		<div class="control-group" style="margin-bottom: 5px;" id="ac" runat="server">
			<label class="control-label" for="amount">Withdraw amount</label>
			<div class="controls" id="acs" runat="server">
				<input type="text" id="amount" name="amount" />
			</div>
		</div>
		<div class="control-group" style="margin-bottom: 5px;" id="adc" runat="server">
			<label class="control-label" for="address">DOGE address</label>
			<div class="controls" id="adcs" runat="server">
				<input type="text" id="address" name="address" />
			</div>
		</div>
		<div class="control-group" style="margin-bottom: 5px;" id="pc" runat="server">
			<label class="control-label" for="password">Your password</label>
			<div class="controls" id="pcs" runat="server">
				<input type="password" id="password" name="password" />
			</div>
		</div>
		<div class="control-group" style="margin-bottom: 5px;" id="bc" runat="server">
			<div class="controls" id="bcs" runat="server">
				<button class="btn btn-success" type="submit">Withdraw!</button>
			</div>
		</div>
		<small>Withdrawing has a 2% fee.</small>
	</form>
			<div class="well pull-right span2">
			<b>Your balance is <h1 id="balance" runat="server"></h1> DOGE.</b>
		</div>
</div>
</asp:Content>