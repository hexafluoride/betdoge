
using System;
using System.Web;
using System.Web.UI;

namespace BetDogeProper
{
	public partial class BetGateway : System.Web.UI.Page
	{
		public void Page_Load ()
		{
			HelperThings h = new HelperThings (Request, Response);
			if (!h.IsLoggedIn ()) {
				Response.Write ("{\"result\":\"Not logged in or invalid session.\"}");
				return;
			}
			float odds = 0;
			float amount = 0;
			if (!float.TryParse (Request.Form ["odds"], out odds) || !float.TryParse (Request.Form ["amount"], out amount)) {
				Response.Write ("{\"result\":\"Invalid request.\"}");
				return;
			}
			if (amount > 1000) {
				Response.Write ("{\"result\":\"Bet too high. Maximum bet amount is 1000 DOGE.\"}");
				return;
			}
//			if ((amount * 100 / (odds - 2)) > (h.bc.GetBalance () / 3) * 2) {
//				Response.Write ("{\"result\":\"You are not allowed to bet with a possible profit bigger than our wallet balance.\"}");
//				return;
//			}
			Response.Write (h.Bet (h.GetCookie ("username"), odds, amount));
		}
	}
}

