using System.Linq;
using System;
using System.Web;
using System.Web.UI;

namespace BetDogeProper
{
	public partial class GetBets : System.Web.UI.Page
	{
		public void Page_Load()
		{
			HelperThings h = new HelperThings(Request, Response);
			int count = int.Parse (Request.Params["count"]);
			if(count > 7)
				return;
			bool hidden = true;
			if(Request.Params.AllKeys.Contains ("hidden"))
				hidden = bool.Parse (Request.Params["hidden"]);
			Response.Write (h.GetBets (count, hidden));
		}
	}
}

