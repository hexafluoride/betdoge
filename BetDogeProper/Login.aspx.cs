using MySql.Data.MySqlClient;
using System;
using System.Web;
using System.Web.UI;

namespace BetDogeProper
{
	public partial class Login : System.Web.UI.Page
	{
		HelperThings help;
		public void Page_Load ()
		{
			if (Request.Params ["error"] == "1") {
				alert.Attributes ["class"] = "alert alert-error";
				alert.InnerHtml = "<strong>Error!</strong> Check your login information.";
			}
			//help = new HelperThings(Request, Response);
		}
		public void Signin ()
		{
			string s = help.GenerateSession (username.Value, password.Value);
			if (s == "-") {
				alert.Attributes ["class"] = "alert alert-error";
				alert.InnerHtml = "<strong>Error!</strong> Check your login information.";
			} else {
				help.SetCookie ("username", username.Value);
				help.SetCookie ("sessionid", s);
				help.Redirect ("/Default.aspx");
			}
		}
	}
}

